extends Control

var timer

func _ready():
	timer = Timer.new()
	timer.connect("timeout",self,"_on_timer_timeout")
	add_child(timer)

# warning-ignore:unused_argument
func _on_RayCast_printText(extra_arg_0, extra_arg_1=null):
	$TextBackground/Textbox.set_text(extra_arg_0)
	$TextBackground.visible = true
	timer.start(1)
	
func _on_timer_timeout():
	$TextBackground/Textbox.set_text("")
	$TextBackground.visible = false
	timer.stop()