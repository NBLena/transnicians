extends Control

export var messages = [["Why are there so many trans women in tech?", "The following is a theory."],
		["It seems, tech was an avenue boys were encouraged to explore.", "And so they did. Especially the shy ones. With Anxiety.", "And disphoria."],
		["There is nothing male about tech, however.", "Nor was there about these boys, as they would figure out."],
		["And so these women lived truly.", "Or at least I would."],
		["And so my theory goes.", "I realized in my mid twenties that something was up with my gender."],
		["I'm still getting there.", "Created by Lena Siess", "E-Mail: lena@techromancer.club", "YouTube: Gescheit Gespielt", "Thank you for playing."]]

var act
var counter = 1

func _ready():
	act = Global.act
	$Label.text = messages[act][0] # load first message in already

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if counter < len(messages[act]) :
			# Advance scene
			$Label.text = messages[act][counter]
			counter += 1
		else:
			Global.act += 1
			chooseNextScene()
			
	# Function to close game.
	if Input.is_key_pressed(KEY_ESCAPE) or Input.is_key_pressed(KEY_Q):
		get_tree().quit()

func chooseNextScene():
	match Global.act:
		1, 4:
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://scenes/Child.tscn")
		2, 5:
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://scenes/Adolescance.tscn")
		3:
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://scenes/Dream.tscn")
		6:
			get_tree().quit()