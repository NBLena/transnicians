extends RayCast

signal printText

# warning-ignore:unused_argument
func _physics_process(delta):
	if is_colliding():
		var object = get_collider()
		var messageGroup = "message"
		if Global.act > 3:
			messageGroup = "message2"
		if object.get_parent().get(messageGroup) != null:
			if Global.act > 3:
				emit_signal("printText", object.get_parent().message2)
			else:
				emit_signal("printText", object.get_parent().message)
			if get_parent().get_parent().get_node('.').currentObject == null:
				if object.get_class() == "RigidBody":
					get_parent().get_parent().get_node('.').currentObject = object
			object.highlight()
		else:
			push_error("ERROR: Object focused but no message was declared!")
	else:
		get_parent().get_parent().get_node('.').currentObject = null