extends RigidBody

onready var highlightMaterial = ResourceLoader.load("res://materials/highlight.tres")
onready var defaultMaterial = ResourceLoader.load("res://materials/default.tres")
onready var defaultSpatialMaterial = ResourceLoader.load("res://materials/default_spatial.tres")
var materialMeshes = []
var timer

func _ready():
	if $Mesh.get_class() == 'MeshInstance':
		materialMeshes.append($Mesh)
	else:
		for child in $Mesh.get_children():
			if child.get_class() == 'MeshInstance':
				materialMeshes.append(child)
	
	timer = Timer.new()
	timer.connect("timeout",self,"_on_timer_timeout")
	add_child(timer)

func highlight():
	for mesh in materialMeshes:
		var currentMaterial = mesh.mesh.surface_get_material(0)
		if is_default_material(currentMaterial):
			mesh.set_surface_material(0,highlightMaterial)
		else:
			currentMaterial.set_albedo(Color(0.99,0.80,0.24))
	
	timer.start(0.2)

func _on_timer_timeout():
	for mesh in materialMeshes:
		var currentMaterial = mesh.mesh.surface_get_material(0)
		if is_default_material(currentMaterial):
			mesh.set_surface_material(0, null)
		else:
			currentMaterial.set_albedo(Color(1,1,1))
	
	timer.stop()

func is_default_material(currentMaterial):
	return currentMaterial == defaultMaterial or currentMaterial == defaultSpatialMaterial