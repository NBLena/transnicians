extends Spatial

# warning-ignore:unused_class_variable
export var message = "Key for a decentralized identity system. No-one should own your online presence."