# Based on the code in "First Person Controller" and "SimpleFirstPerson" by bdashraful and aarroz
# Licensed via MIT
# Modified by me

extends KinematicBody
#Variables

var speed = 5
var MOUSESPEED = 0.005
var velocity = Vector3(0,0,0)
var Walk_Speed = 0
var Maximum_Walk_Speed = 10

var currentObject = null
var heldItem = null

var blockPlayer = false

onready var holdPosition = get_node("Camera/HoldPosition")
var animationsObject = null

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	set_physics_process(true)
	set_process_input(true)
	match Global.act:
		1, 4:
			animationsObject = get_parent().get_node("Room/AnimationFloppy")
		2, 5:
			animationsObject = get_parent().get_node("Animation_Cd")
		3:
			animationsObject = get_parent().get_node("Animation_Stick")
	if animationsObject == null:
		print ("ERROR: Could not animation object for this act!")
		return

func _input(event):
	# For Mouse Look. The Camera node has a script for the X rotation.
	
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x*MOUSESPEED
		
	# Hold an object in front of you (or drop it)
	if event.is_action_pressed("ui_accept") and (currentObject != null or (currentObject == null and heldItem != null)):
		if heldItem == null:
			pickup()
		else:
			drop()
			
	# Function to close game.
	if Input.is_key_pressed(KEY_ESCAPE) or Input.is_key_pressed(KEY_Q):
		get_tree().quit()

# warning-ignore:unused_argument
func _process(delta):
	velocity = Vector3(0,0,0)
	
	if not blockPlayer:
		#Section for basic movement.
		if Input.is_action_pressed("ui_up"):
			Walk_Speed += speed
			if Walk_Speed > Maximum_Walk_Speed:
				Walk_Speed = Maximum_Walk_Speed
			velocity.x += global_transform.basis.z.x * Walk_Speed
			velocity.z += global_transform.basis.z.z * Walk_Speed
		if Input.is_action_pressed("ui_down"):
			Walk_Speed += speed
			if Walk_Speed > Maximum_Walk_Speed:
				Walk_Speed = Maximum_Walk_Speed
			velocity.x -= global_transform.basis.z.x * Walk_Speed
			velocity.z -= global_transform.basis.z.z * Walk_Speed
		if Input.is_action_pressed("ui_left"):
			Walk_Speed += speed
			if Walk_Speed > Maximum_Walk_Speed:
				Walk_Speed = Maximum_Walk_Speed
			velocity.x += global_transform.basis.x.x * Walk_Speed
			velocity.z += global_transform.basis.x.z * Walk_Speed
		if Input.is_action_pressed("ui_right"):
			Walk_Speed += speed
			if Walk_Speed > Maximum_Walk_Speed:
				Walk_Speed = Maximum_Walk_Speed
			velocity.x -= global_transform.basis.x.x * Walk_Speed
			velocity.z -= global_transform.basis.x.z * Walk_Speed
			
		if not(Input.is_key_pressed(KEY_W) or Input.is_key_pressed(KEY_A) or Input.is_key_pressed(KEY_S) or Input.is_key_pressed(KEY_D) or Input.is_key_pressed(KEY_UP) or Input.is_key_pressed(KEY_DOWN) or Input.is_key_pressed(KEY_LEFT) or Input.is_key_pressed(KEY_RIGHT)):
			velocity.x = 0
			velocity.z = 0
			
		if heldItem:
			heldItem.global_transform.origin = holdPosition.global_transform.origin
		
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
func pickup():
	heldItem = currentObject # currentObject is set in InteractionController.gd
	heldItem.sleeping = true

func drop():
	heldItem.sleeping = false
	heldItem.set_linear_velocity(Vector3(0,0,0))
	heldItem = null

func useObject():
	if heldItem != null:
			blockPlayer = true
			$Camera/RayCast.enabled = false
			heldItem.visible = false
			animationsObject.visible = true
			var animation = null
			match Global.act:
				1, 4:
					animation = "floppy_insert"
				2, 5:
					animation = "insert_cd"
				3:
					animation = "insert_stick"
			if animation == null:
				print ("ERROR: Could not decide animation to play!")
				return
			get_parent().get_node("AnimationPlayer").play(animation)

func _on_GoalArea_body_entered(body):
	var keyObject = null

	match Global.act:
		1, 4: 
			keyObject = get_parent().get_node("Room/Floppy/RigidBody")
		2, 5:
			keyObject = get_parent().get_node("Interactibles/Cd/RigidBody")
		3:
			keyObject = get_parent().get_node("Interactibles/USB_Stick/RigidBody")
	if keyObject == null:
		print("ERROR: Could not load keyobject for check")
		return
	if body == keyObject:
		useObject()

func changeSceneToTransition():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Transition.tscn")